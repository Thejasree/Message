package com.wavelabs.repos;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.entity.User;

public interface UserRepo extends CrudRepository<User, Integer> {


	public User findById(Long id);
	Boolean  findByUuidAndTenantId(String uuid,String tenantId);
	
}
