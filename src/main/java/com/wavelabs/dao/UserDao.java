package com.wavelabs.dao;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.entity.User;
import com.wavelabs.repos.UserRepo;

@Component
public class UserDao {
	@Autowired
	UserRepo userRepo;

	static Logger logger = Logger.getLogger(UserDao.class);

	private UserDao() {

	}

	public User getUser(long id) {

		return userRepo.findById(id);

	}

	public boolean saveUser(User user) {

		try {
			userRepo.save(user);
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		}
	}

	public boolean deleteUser(int id) {
		try {
			userRepo.delete(id);
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		}

	}
}
