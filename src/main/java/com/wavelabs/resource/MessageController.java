package com.wavelabs.resource;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.entity.Message;
import com.wavelabs.entity.UserTokenInfo;
import com.wavelabs.services.MessageService;
import com.wavelabs.services.UserTypeChecker;

import io.nbos.capi.api.v0.models.RestMessage;

@RestController
@Component
@RequestMapping(value = "/Messages")
public class MessageController {
	
	@Autowired
	MessageService messageService;
	RestMessage rm  = new RestMessage();
	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity createMessage(@RequestBody Message message,
			@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {
		Boolean flag = UserTypeChecker.isGuestUser(userTokenInfo);
		RestMessage rm = new RestMessage();
		if (!flag) {
			Boolean flag1 = messageService.saveMessage(message);
			
			if (flag1) {
				rm.message = "Message created successfully!!";
				rm.messageCode = "200";
				return ResponseEntity.status(200).body(rm);

			} else {
				rm.messageCode = "500";
				rm.message = "Message creation failed!!";
				return ResponseEntity.status(500).body(rm);
			}
		} else {
			rm.message="Please login into application!";
			rm.messageCode="403";
			return ResponseEntity.status(403).body(rm);
		}

	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity getConversations(@PathVariable("id") long id,
			@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {
		Boolean flag = UserTypeChecker.isGuestUser(userTokenInfo);
		RestMessage rm  = new RestMessage();
		if (!flag) {
			Message[] msg = messageService.getLatestMessageFromAllConversations(id);
	
			if (msg.length !=0) {

				return ResponseEntity.status(200).body(msg);
			} else {
				rm.message="Not found";
				rm.messageCode="404";
				return ResponseEntity.status(404).body(rm);
			}
		} else {
			rm.message="Please login into application...";
			rm.messageCode="403";
			return ResponseEntity.status(403).body(rm);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/list/{id}", method = RequestMethod.GET)
	public ResponseEntity getConversationMessages(@PathVariable("id") Integer id,
			@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {
		Boolean flag = UserTypeChecker.isGuestUser(userTokenInfo);
		RestMessage rm = new RestMessage();
		if (!flag) {
			Message[] m = messageService.getConversationMessages(id);
			if (m==null) {
				rm.message="Failed to retrieve messages!!";
				rm.messageCode="500";
				return ResponseEntity.status(500).body(rm);
			}
			return ResponseEntity.status(200).body(m);
		} else {
			rm.message="Please login into app!!";
			rm.messageCode="403";
			return ResponseEntity.status(403).body(rm);
		}
	}

	

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{userId}/{messageThreadId}", method = RequestMethod.DELETE)
	public ResponseEntity deleteUserConversation(@PathVariable("userId") Integer userId,
			@PathVariable("messageThreadId") int messageThreadId,
			@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {
		Boolean flag1 = UserTypeChecker.isGuestUser(userTokenInfo);
		if (!flag1) {
			 messageService.deleteUserConversation(userId, messageThreadId);
			rm.message = "Message deletion sucess!!";
			rm.messageCode="200";
			return ResponseEntity.status(200).body(rm);
		} else {
			rm.message = "Please login into app.!!";
			rm.messageCode="403";
			return ResponseEntity.status(403).body(rm);

		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity deleteMessage(@PathVariable("id") Integer id,
			@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {
		Boolean flag1 = UserTypeChecker.isGuestUser(userTokenInfo);
		if (!flag1) {
			Boolean flag = messageService.deleteMessage(id);
			if (!flag) {
				rm.message = "Deletion failed!!";
				rm.messageCode="500";
				return ResponseEntity.status(500).body(rm);
			}
			rm.message = "Deletion sucess!!";
			rm.messageCode="200";
			return ResponseEntity.status(200).body(rm);
		} else {
			rm.message = "Please login into app.!!";
			rm.messageCode="403";
			return ResponseEntity.status(403).body(rm);

		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/hit", method = RequestMethod.GET)
	public ResponseEntity getLinkToProceed(@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {
		Boolean flag = messageService.initialClickToSendMessage(userTokenInfo);
		if (flag) {
			rm.message = "Successfully loged in...click next to continue...!!";
			rm.messageCode="200";
			return ResponseEntity.status(200).body(rm);
		} else {
			rm.message = "Please click again!!";
			rm.messageCode="403";
			return ResponseEntity.status(403).body(rm);
		}
	}

}