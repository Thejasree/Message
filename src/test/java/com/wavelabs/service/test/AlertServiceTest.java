package com.wavelabs.service.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;
import com.wavelabs.entity.Alert;
import com.wavelabs.entity.User;
import com.wavelabs.model.test.ObjectBuilder;
import com.wavelabs.repos.AlertRepo;
import com.wavelabs.repos.UserRepo;
import com.wavelabs.services.AlertService;

@RunWith(MockitoJUnitRunner.class)
public class AlertServiceTest {
	@Mock
	private AlertRepo alertRepo;
	@Mock
	UserRepo userRepo;
	@InjectMocks
	private AlertService alertService;

	@Test
	public void testPersistAlert() {
		Alert alert = ObjectBuilder.getAlert();
		when(alertRepo.save(any(Alert.class))).thenReturn(alert);
		Boolean flag = alertService.saveAlert(alert);
		Assert.assertEquals(true, flag);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testPersistAlertForFalse() {
		Alert alert = ObjectBuilder.getAlert();
		when(alertRepo.save(any(Alert.class))).thenThrow(Exception.class);
		Boolean flag = alertService.saveAlert(alert);
		Assert.assertEquals(false, flag);
	}

	@Test
	public void testGetAlert() {
		Alert alert = ObjectBuilder.getAlert();
		when(alertRepo.findAlertById(anyInt())).thenReturn(alert);
		Alert a = alertService.getAlert(anyInt());
		Gson gson = new Gson();
		Assert.assertEquals(gson.toJson(alert), gson.toJson(a));
	}

	@Test
	public void testGetAlertForNull() {
		when(alertRepo.findAlertById(anyInt())).thenReturn(null);
		Alert alert1 = alertService.getAlert(anyInt());
		Assert.assertEquals(null, alert1);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetAlertForCatch() {
		ObjectBuilder.getAlert();
		when(alertRepo.findAlertById(anyInt())).thenThrow(Exception.class);
		alertService.getAlert(anyInt());
	}

	@Test
	public void testGetAlertAndUpdate() {
		Alert alert = ObjectBuilder.getAlert();
		when(alertRepo.save(any(Alert.class))).thenReturn(alert);
		Alert alert1 = alertService.updateAlert(alert);
		Gson gson = new Gson();
		Assert.assertEquals(gson.toJson(alert), gson.toJson(alert1));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetAlertAndUpdateForCatch() {
		Alert alert = ObjectBuilder.getAlert();
		when(alertRepo.save(any(Alert.class))).thenThrow(Exception.class);
		Alert alert1 = alertService.updateAlert(alert);

	}

	@Test
	public void testGetAlertAndUpdateForNull() {
		Alert alert = ObjectBuilder.getAlert();
		when(alertRepo.save(any(Alert.class))).thenReturn(null);
		Alert alert1 = alertService.updateAlert(alert);
		Gson gson = new Gson();
		Assert.assertEquals(gson.toJson(alert), gson.toJson(alert1));
	}

	@Test
	public void testgetAllAlertsPerReceiver() {
		User user = ObjectBuilder.getUser();
		when(userRepo.findById(anyLong())).thenReturn(user);
		@SuppressWarnings("unchecked")
		List<Alert> list = mock(ArrayList.class);
		when(alertRepo.findAllAlertsById(any(User.class))).thenReturn(list);
		List<Alert> l = alertService.getAllAlertsPerReceiver(anyLong());
		Assert.assertEquals(list.size(), l.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testgetAllAlertsPerReceiverForCatch() {
		User user = ObjectBuilder.getUser();
		when(userRepo.findById(anyLong())).thenReturn(user);
		List<Alert> list = mock(ArrayList.class);
		when(alertRepo.findAllAlertsById(any(User.class))).thenThrow(Exception.class);
		List<Alert> l = alertService.getAllAlertsPerReceiver(anyLong());
		Assert.assertEquals(list.size(), l.size());
	}

	@Test
	public void testgetAllAlertsPerReceiverForEmptyList() {
		User user = ObjectBuilder.getUser();
		when(userRepo.findById(anyLong())).thenReturn(user);
		List<Alert> list = Collections.emptyList();
		when(alertRepo.findAllAlertsById(any(User.class))).thenReturn(list);
		List<Alert> l = alertService.getAllAlertsPerReceiver(anyLong());
		Assert.assertEquals(list.size(), l.size());
	}
}
