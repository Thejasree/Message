package com.wavelabs.service.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.entity.Message;
import com.wavelabs.entity.MessageThread;
import com.wavelabs.entity.User;
import com.wavelabs.model.test.ObjectBuilder;
import com.wavelabs.repos.MessageRepo;
import com.wavelabs.repos.MessageThreadRepo;
import com.wavelabs.repos.UserRepo;
import com.wavelabs.services.MessageService;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest {
	@Mock
	UserRepo userRepo;
	@InjectMocks
	MessageService messageService;
	@Mock
	MessageRepo messageRepo;
	@Mock
	MessageThreadRepo messageThreadRepo;

	@Test
	public void testinitialClickToSendMessage() {
		when(messageThreadRepo.findById(anyInt())).thenReturn(mock(MessageThread.class));
		when(userRepo.save(any(User.class))).thenReturn(mock(User.class));
		messageService.initialClickToSendMessage(ObjectBuilder.getUserTokenInfo());
		verify(userRepo, times(1)).save(any(User.class));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testForCatchinitialClickToSendMessage() {
		when(messageThreadRepo.findById(anyInt())).thenReturn(mock(MessageThread.class));
		when(userRepo.save(any(User.class))).thenThrow(Exception.class);
		boolean flag = messageService.initialClickToSendMessage(ObjectBuilder.getUserTokenInfo());
		Assert.assertEquals(false, flag);
	}

	@Test
	public void testPersistMessage() {
		Message message = ObjectBuilder.getMessage();
		message.setMessagethread(null);
		MessageThread mt1 = ObjectBuilder.getMessageThread();
		when(messageThreadRepo.save(mt1)).thenReturn(mt1);
		when(messageRepo.save(any(Message.class))).thenReturn(ObjectBuilder.getMessage());
		Boolean flag = messageService.saveMessage(message);
		Assert.assertEquals(true, flag);

	}

	@Test
	public void testReplyMessage() {
		MessageThread mt = ObjectBuilder.getMessage().getMessagethread();
		when(messageThreadRepo.findById(anyInt())).thenReturn(mt);
		Message m = ObjectBuilder.getMessage();
		when(messageRepo.save(any(Message.class))).thenReturn(m);
		Boolean flag = messageService.saveMessage(m);
		Assert.assertEquals(true, flag);

	}

	@Test
	public void testReplyMessageIsFalse() {
		MessageThread mt = ObjectBuilder.getMessage().getMessagethread();
		when(messageThreadRepo.findById(anyInt())).thenReturn(mt);
		Message m = ObjectBuilder.getMessage();
		when(messageRepo.save(any(Message.class))).thenReturn(null);
		Boolean flag = messageService.saveMessage(m);
		Assert.assertEquals(true, flag);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCatchMethodForSaveMessage() {
		MessageThread mt = ObjectBuilder.getMessage().getMessagethread();
		when(messageThreadRepo.findById(anyInt())).thenReturn(mt);
		Message m = ObjectBuilder.getMessage();
		when(messageRepo.save(any(Message.class))).thenThrow(Exception.class);
		Boolean flag = messageService.saveMessage(m);
		Assert.assertEquals(false, flag);
	}

	@Test
	public void testDeleteMessageConversation() {
		when(messageThreadRepo.findById(anyInt())).thenReturn(ObjectBuilder.getMessageThread());
		when(messageThreadRepo.save(any(MessageThread.class))).thenReturn(ObjectBuilder.getMessageThread());
		Boolean flag = messageService.deleteUserConversation(3l, 2);
		verify(messageThreadRepo, times(1)).save(any(MessageThread.class));
		Assert.assertEquals(true, flag);
	}

	@Test
	public void testDeleteMessageConversation1() {
		MessageThread mt = new MessageThread();
		when(messageThreadRepo.findById(anyInt())).thenReturn(mt);
		when(messageThreadRepo.save(any(MessageThread.class))).thenReturn(ObjectBuilder.getMessageThread());
		Boolean flag = messageService.deleteUserConversation(2l, 2);
		Assert.assertEquals(false, flag);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCatchForDeleteMessageConversation() {
		MessageThread mt = new MessageThread();
		when(messageThreadRepo.findById(anyInt())).thenReturn(mt);
		when(messageThreadRepo.save(any(MessageThread.class))).thenThrow(Exception.class);
		Boolean flag = messageService.deleteUserConversation(5, 2);
		Assert.assertEquals(false, flag);
	}

	@Test
	public void testDeleteMessage() {
		when(messageRepo.delete(anyInt())).thenReturn(true);
		Boolean flag = messageService.deleteMessage(anyInt());
		Assert.assertEquals("not provided", flag, true);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCatchForDeleteMessage() {
		when(messageRepo.delete(anyInt())).thenThrow(Exception.class);
		Boolean flag = messageService.deleteMessage(anyInt());
		Assert.assertEquals("not provided", flag, false);
	}

	@Test
	public void testGetConversations() {
		when(userRepo.findById(anyLong())).thenReturn(ObjectBuilder.getUser());
		List<MessageThread> mts = new ArrayList<>();
		mts.add(ObjectBuilder.getMessageThread());
		when(messageRepo.findByToId(any(User.class))).thenReturn(mts);
		when(messageRepo.findByMessagethread(any(MessageThread.class))).thenReturn(ObjectBuilder.getMessage());
		Message[] msgs = messageService.getLatestMessageFromAllConversations(1);
		Assert.assertEquals(1, msgs.length);
	}

	@Test
	public void testGetConversationMessages() {
		when(messageThreadRepo.findOne(anyInt())).thenReturn(ObjectBuilder.getMessageThread());
		when(messageRepo.findAllMessagesOfGivenThread(any(MessageThread.class)))
				.thenReturn(new Message[] { ObjectBuilder.getMessage() });
		Message[] msgs = messageService.getConversationMessages(2);
		Assert.assertEquals(1, msgs.length);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetConversationMessagesForCatch() {
		when(messageThreadRepo.findOne(anyInt())).thenThrow(Exception.class);
		when(messageRepo.findAllMessagesOfGivenThread(any(MessageThread.class)))
				.thenReturn(new Message[] { ObjectBuilder.getMessage() });
		Message[] msgs = messageService.getConversationMessages(2);
		Assert.assertEquals(0, msgs.length);
	}
}
