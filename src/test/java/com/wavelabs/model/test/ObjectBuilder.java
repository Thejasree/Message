package com.wavelabs.model.test;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import com.wavelabs.entity.Alert;
import com.wavelabs.entity.AlertMetadata;
import com.wavelabs.entity.Member;
import com.wavelabs.entity.Message;
import com.wavelabs.entity.MessageThread;
import com.wavelabs.entity.Modules;
import com.wavelabs.entity.User;
import com.wavelabs.entity.UserTokenInfo;
import com.wavelabs.models.enums.AlertType;
import com.wavelabs.models.enums.Status;

public class ObjectBuilder {
	public static Modules getModules() {
		Modules mod = new Modules();
		mod.setName("Messaging");
		mod.setUuid("abc123");
		return mod;

	}

	public static Member getMember() {
		Member mem = new Member();
		mem.setUuid("ab12");
		return mem;
	}

	public static User getUser() {
		User user = new User();
		user.setId(2l);
		user.setTenantId("abc123");
		user.setUserName("Anu");
		user.setUuid("abcd1234");
		return user;

	}
	public static User getUser1() {
		User user1 = new User();
		user1.setId(3l);
		user1.setTenantId("abc123");
		user1.setUserName("Janu");
		user1.setUuid("abcd1234");
		return user1;

	}
	
	public static UserTokenInfo getUserTokenInfo() {

		UserTokenInfo userToken = new UserTokenInfo();
		userToken.setClientId("aaa");
		userToken.setEmail("aaa@gmail.com");
		userToken.setExpiration(Calendar.getInstance());
		userToken.setExpired(false);
		userToken.setId(2l);
		userToken.setModules(new Modules[] { getModules() });
		userToken.setMember(getMember());
		userToken.setPhone("1234567890");
		userToken.setTenantId("abc12345");
		userToken.setToken("token123");
		userToken.setTokenType("user");
		userToken.setUsername("Janu");
		userToken.setExpired(false);
		return userToken;

	}

	public static MessageThread getMessageThread() {
		MessageThread mt = new MessageThread();
		mt.setIsread(true);
		Set<User> set = new HashSet<User>();
		set.add(getUser());
		set.add(getUser1());
		mt.setUser(set);
		return mt;

	}

	public static Message getMessage() {
		Message message = new Message();
		message.setId(2);
		message.setFromId(getUser());
		message.setIs_reply(false);
		message.setMessagethread(getMessageThread());
		message.setSubject("Hi.");
		message.setTenantId("ab12");
		message.setTimeStamp(Calendar.getInstance());
		message.setToId(getUser());
		message.setBody("Hey, where are you?");
		return message;

	}

	public static AlertMetadata getAlertMetadata() {
		AlertMetadata am = new AlertMetadata();
		am.setAlertType(AlertType.ACCEPTDECLINE);
		am.setId(2);
		return am;

	}

	public static Alert getAlert() {
		Alert alert = new Alert();
		alert.setId(4);
		alert.setFromId(getUser());
		alert.setStatus(Status.ACCEPT);
		alert.setTenantId("ab12");
		alert.setText("Contact request sent");
		alert.setToId(getUser());
		alert.setMetadata(getAlertMetadata());
		return alert;
	}

}
