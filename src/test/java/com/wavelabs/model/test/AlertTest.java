package com.wavelabs.model.test;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.wavelabs.entity.Alert;
import com.wavelabs.entity.AlertMetadata;
import com.wavelabs.entity.User;
import com.wavelabs.models.enums.Status;

public class AlertTest {
	Alert alert = new Alert();
	User user = new User();
	AlertMetadata metadata = new AlertMetadata();

	@Test
	public void testDefaultConstructor() {
		alert.setFromId(user);
		assertEquals(user,alert.getFromId());
		alert.setId(2);
		assertEquals(2, alert.getId());
		alert.setMetadata(metadata);
		assertEquals(metadata, alert.getMetadata());
		alert.setStatus(Status.ACCEPT);
		assertEquals(Status.ACCEPT, alert.getStatus());
		alert.setText("Send Request");
		assertEquals("Send Request", alert.getText());
		alert.setToId(user);
		assertEquals(user, alert.getToId());
		
		
	}
	@Test
	public void testTenantId(){
		Alert a = new Alert();
		a.setTenantId("abc123");
		assertEquals("abc123", a.getTenantId());
	}
	
	@Test
	public void testId(){
		alert.setId(2);
		assertEquals("not given", alert.getId() ==2, true);
	}
	@Test
	public void testFromId(){
		alert.setFromId(user);
		assertEquals("not given", alert.getFromId() == user, true);
		
	}
	@Test
	public void testToId(){
		alert.setToId(user);
		assertEquals("not given", alert.getToId() == user, true);
	}
	@Test
	public void testText(){
		alert.setText("Request");
		assertEquals("not given", alert.getText() == "Request", true);
	}
	@Test
	public void testMetadata(){
		alert.setMetadata(metadata);;
		assertEquals("not given", alert.getMetadata() == metadata, true);
		
	}
	@Test
	public void testStatus(){
		alert.setStatus(Status.ACCEPT);
		alert.setStatus(Status.DECLINE);
		alert.setStatus(Status.VIEWED);
		assertEquals("not given", alert.getStatus().equals("ACCEPT"), false);
		assertEquals("not given", alert.getStatus().equals("DECLINE"), false);
		assertEquals("not given", alert.getStatus().equals("VIEWED"), false);
		
	}
}
	
