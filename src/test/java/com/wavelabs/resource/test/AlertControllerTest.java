package com.wavelabs.resource.test;




import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.entity.Alert;
import com.wavelabs.entity.UserTokenInfo;
import com.wavelabs.model.test.ObjectBuilder;
import com.wavelabs.resource.AlertController;
import com.wavelabs.services.AlertService;
import com.wavelabs.services.UserTypeChecker;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UserTypeChecker.class)
public class AlertControllerTest {
	@Mock
	private AlertService alertService;
	@InjectMocks
	private AlertController alertResource;

	@Test
	public void testPersistAlert() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		Alert alert = ObjectBuilder.getAlert();
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		when(alertService.saveAlert(any())).thenReturn(true);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = alertResource.saveAlert(alert, userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());
		
	}

	@Test
	public void testPersistAlertIsNull() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		Alert alert = ObjectBuilder.getAlert();
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		when(alertService.saveAlert(any())).thenReturn(false);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = alertResource.saveAlert(alert, userTokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());
	}
	@Test
	public void testPersistAlert1() {
	PowerMockito.mockStatic(UserTypeChecker.class);
	UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
	Alert alert = ObjectBuilder.getAlert();
	when(UserTypeChecker.isGuestUser(any())).thenReturn(true);
	when(alertService.saveAlert(any())).thenReturn(false);
	@SuppressWarnings("rawtypes")
	ResponseEntity entity = alertResource.saveAlert(alert, userTokenInfo);
	Assert.assertEquals(403, entity.getStatusCodeValue());
	}
	@SuppressWarnings("rawtypes")
	@Test
	public void testGetAlert() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		Alert alert = ObjectBuilder.getAlert();
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(false);
		when(alertService.getAlert(anyInt())).thenReturn(alert);
		ResponseEntity entity = alertResource.getAlert(3, userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());

	}

	@Test
	public void testGetAlertIsNull() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(false);
		when(alertService.getAlert(anyInt())).thenReturn(null);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = alertResource.getAlert(3, userTokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());

	}
	@SuppressWarnings("rawtypes")
	@Test
	public void testGetAlert1() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		Alert alert = ObjectBuilder.getAlert();
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(true);
		when(alertService.getAlert(anyInt())).thenReturn(alert);
		ResponseEntity entity = alertResource.getAlert(3, userTokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());

	}
	@Test
	public void testGetAllAlertsAsPerReceiver() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		Alert alert = ObjectBuilder.getAlert();
		List<Alert> aletrs = new ArrayList<>();
		aletrs.add(alert);
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(false);
		when(alertService.getAllAlertsPerReceiver(anyInt())).thenReturn(aletrs);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = alertResource.getAllAlertsPerReceiver(2, userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());

	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetAllAlertsAsPerReceiverIsEmpty() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		Alert alert = ObjectBuilder.getAlert();
		List<Alert> alerts =  new ArrayList<>();
		alerts.add(alert);
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(false);
		when(alertService.getAllAlertsPerReceiver(anyInt())).thenReturn(alerts);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = alertResource.getAllAlertsPerReceiver(2, userTokenInfo);
		List<Alert> a = (List<Alert>) entity.getBody();
		Boolean flag = a.isEmpty();
		Assert.assertEquals(false, flag);
		//Assert.assertEquals(500, entity.getStatusCodeValue());
		
	}

	@Test
	public void testGetAllAlertsAsPerReceiverIsTrue() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		Alert alert = ObjectBuilder.getAlert();
		List<Alert> alerts =  new ArrayList<>();
		alerts.add(alert);
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(true);
		when(alertService.getAllAlertsPerReceiver(anyInt())).thenReturn(alerts);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = alertResource.getAllAlertsPerReceiver(2, userTokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());
		
		
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testGetAlertAndUpdate() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		Alert alert = ObjectBuilder.getAlert();
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(false);
		when(alertService.updateAlert(alert)).thenReturn(alert);
		ResponseEntity entity = alertResource.getAlertAndUpdate(2, alert, userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testGetAlertAndUpdateIsNull() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		Alert alert = ObjectBuilder.getAlert();
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(false);
		when(alertService.updateAlert(alert)).thenReturn(null);
		ResponseEntity entity = alertResource.getAlertAndUpdate(2, alert, userTokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());
	}
	@SuppressWarnings("rawtypes")
	@Test
	public void testGetAlertAndUpdateIsTrue() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		Alert alert = ObjectBuilder.getAlert();
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(true);
		when(alertService.updateAlert(alert)).thenReturn(alert);
		ResponseEntity entity = alertResource.getAlertAndUpdate(2, alert, userTokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());
	}
}
