CREATE DATABASE IF NOT EXISTS nbos1;

use nbos1;

DROP TABLE IF EXISTS `alert_metadata`;
CREATE TABLE `alert_metadata` (
  `id` int(11) NOT NULL,
  `alert_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `alert`;

CREATE TABLE `alert` (
  `id` int(11) NOT NULL,
  `Text` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `alert_metadata_id` int(11) DEFAULT NULL,
  `from_id` int(11) DEFAULT NULL,
  `to_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcui4cg7ov61rpog3xumi985ay` (`alert_metadata_id`),
  KEY `FK7khcve8lmqs30hsfxgxn3edb7` (`from_id`),
  KEY `FKf9bcm14eu9yuvsicj9d72ypx6` (`to_id`),
  CONSTRAINT `FK7khcve8lmqs30hsfxgxn3edb7` FOREIGN KEY (`from_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKcui4cg7ov61rpog3xumi985ay` FOREIGN KEY (`alert_metadata_id`) REFERENCES `alert_metadata` (`id`),
  CONSTRAINT `FKf9bcm14eu9yuvsicj9d72ypx6` FOREIGN KEY (`to_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` varchar(255) DEFAULT NULL,
  `is_reply` bit(1) DEFAULT NULL,
  `timeStamp_db` datetime DEFAULT NULL,
  `message_thread_id` int(11) DEFAULT NULL,
  `from_Id` int(11) DEFAULT NULL,
  `to_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK77sdf950j24brc05remm0s0wk` (`message_thread_id`),
  KEY `FKej5yoh68ftupj27hhu9ch2dy8` (`from_Id`),
  KEY `FKpgm0a8tid7o2j6c8w8fpy77fu` (`to_id`),
  CONSTRAINT `FK77sdf950j24brc05remm0s0wk` FOREIGN KEY (`message_thread_id`) REFERENCES `messagethread` (`id`),
  CONSTRAINT `FKej5yoh68ftupj27hhu9ch2dy8` FOREIGN KEY (`from_Id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKmyqbi59ejhsg0g4o0qxe2lqty` FOREIGN KEY (`to_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKpgm0a8tid7o2j6c8w8fpy77fu` FOREIGN KEY (`to_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `messagethread`;

CREATE TABLE `messagethread` (
  `id` int(11) NOT NULL,
  `is_read` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_messagethread`;

CREATE TABLE `user_messagethread` (
  `message_thread_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`message_thread_id`,`user_id`),
  KEY `FK8rydwolsx4g1t1prlm45kl187` (`user_id`),
  CONSTRAINT `FK521y57osla68u6jb6aerm8g46` FOREIGN KEY (`message_thread_id`) REFERENCES `messagethread` (`id`),
  CONSTRAINT `FK8rydwolsx4g1t1prlm45kl187` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;